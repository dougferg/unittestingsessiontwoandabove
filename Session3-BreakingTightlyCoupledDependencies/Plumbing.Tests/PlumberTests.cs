﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using PlumberImpl;

namespace Plumbing.Tests
{
    [TestFixture]
    public class PlumberTests
    {
        
        private Mock<INotificationService> _notificationService;
        private Mock<IPipeService> _pipeProvider;
        private const string JobNumber = "jobno";

        [SetUp]
        public void Init()
        {
            _notificationService = new Mock<INotificationService>();
            _pipeProvider = new Mock<IPipeService>();
            
        }

        [Test]
        public void DoPlumbing_gets_pipe_for_jobnumber()
        {
            //Arrange
            Plumber sut = new Plumber(_notificationService.Object, _pipeProvider.Object);

            _pipeProvider.Setup(x => x.GetPipeForJob(JobNumber)).Returns(new List<Pipe>());
            
            //Act
            sut.DoPlumbing(JobNumber);

            //Assert
            _pipeProvider.VerifyAll();
        }

        [Test]
        public void DoPlumbing_sends_good_morning_message_if_hourLessThan12()
        {
            //Arrange
            EasierToTestPlumber sut = new EasierToTestPlumber(_notificationService.Object, _pipeProvider.Object);

            _pipeProvider.Setup(x => x.GetPipeForJob(JobNumber)).Returns(new List<Pipe>());
            var tod = new DateTime(2014, 10, 10, 10, 10, 10);
            sut.SetTimeOfDay(tod);

            //Act
            sut.DoPlumbing(JobNumber);

            //Assert
            _notificationService.Verify(x=>x.SendNotification(It.Is<string>(y=>y.StartsWith("Good Morning"))));
        }
    }

    //Only used for testing.  Override problematic members of the system under test.
    public class EasierToTestPlumber : Plumber
    {
        private DateTime _tod;

        public EasierToTestPlumber(INotificationService notificationService) : base(notificationService)
        {
        }

        public EasierToTestPlumber(INotificationService notificationService, IPipeService pipeProvider) : base(notificationService, pipeProvider)
        {
        }

        public void SetTimeOfDay(DateTime tod)
        {
            _tod = tod;
        }

        protected override DateTime GetDateTime()
        {
            if (_tod == System.DateTime.MinValue)
            {
                throw new Exception("Silly developer.  You have to initialize this before you can use it.");
            }
            return _tod;
        }
    }
}
