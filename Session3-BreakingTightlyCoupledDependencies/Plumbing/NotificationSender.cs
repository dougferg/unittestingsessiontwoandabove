﻿namespace PlumberImpl
{
    public interface INotificationService
    {
        void SendNotification(string message);
    }

    public class NotificationService : INotificationService
    {
        public void SendNotification(string message)
        {
            
        }
    }
}