﻿using System.Collections.Generic;

namespace Plumbing
{
    public class PipeServiceWrapper : IPipeService
    {
        public IList<Pipe> GetPipeForJob(string jobNumber)
        {
            return DAL.GetPipeForJob(jobNumber);
        }
    }
}