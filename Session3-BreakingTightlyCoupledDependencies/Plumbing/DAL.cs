﻿using System.Collections.Generic;

namespace Plumbing
{
    public static class DAL
    {
        public static IList<Pipe> GetPipeForJob(string jobNumber)
        {
            var pipes  = new List<Pipe>();

            if (jobNumber == "5")
            {
                pipes.Add(new Pipe("job5Pipe1"));
                pipes.Add(new Pipe("job5Pipe2"));
                pipes.Add(new Pipe("job5Pipe3"));
            }

            if (jobNumber == "10")
            {
                pipes.Add(new Pipe("job10Pipe1"));
                pipes.Add(new Pipe("job10Pipe2"));
                pipes.Add(new Pipe("job10Pipe3"));
            }

            else
            {
                pipes.Add(new Pipe("???"));
            }

            return pipes;
        }
    }
}