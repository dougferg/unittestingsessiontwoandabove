﻿using System;
using System.Collections.Generic;
using System.Text;
using PlumberImpl;

namespace Plumbing
{
    public class Plumber
    {
        private readonly INotificationService _notificationService;
        private readonly IPipeService _pipeService;


        //Having both a "real" ctor and one that knows about creating its dependencies is bad practice.  In fact, the term for it is "bastard injection".
        //This is in place as an interim step only.  It will allow you to get this under test while converting any hard references to it that may depend on the previous single parameter constructor.
        //Once any code using the single ctor has been fixed, this particular ctor should be removed.
        public Plumber(INotificationService notificationService): this(notificationService, new PipeServiceWrapper())
        {
            if (notificationService == null) throw new ArgumentNullException("notificationService");
            _notificationService = notificationService;
        }

        public Plumber(INotificationService notificationService, IPipeService pipeService)
        {
            if (notificationService == null) throw new ArgumentNullException("notificationService");
            if (pipeService == null) throw new ArgumentNullException("pipeService");
            _notificationService = notificationService;
            _pipeService = pipeService;
        }

        public void DoPlumbing(string jobNumber)
        {
            IList<Pipe> pipes = _pipeService.GetPipeForJob(jobNumber);
            //Do plumbing with the pipes.
            SendNotification(jobNumber);
        }

        private void SendNotification(string jobNumber)
        {
            string todGreeting;

            var time = GetDateTime(); 
            if (time.Hour < 12)
            {
                todGreeting = "Good Morning."; 
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                todGreeting = "Good Evening";
            }
            else
            {
                todGreeting = "Good Night";
            }
            
            var sb = new StringBuilder();

            sb.AppendLine(todGreeting);
            sb.Append(jobNumber);
            sb.AppendLine(" is complete.");

            var message = sb.ToString();

            _notificationService.SendNotification(message);

        }

        //Extracted the problematic part so it can be overridden in a test
        protected virtual DateTime GetDateTime()
        {
            return DateTime.Now;
        }
    }
}
