using System.Collections.Generic;

namespace Plumbing
{
    public interface IPipeService
    {
        IList<Pipe> GetPipeForJob(string jobNumber);
    }
}