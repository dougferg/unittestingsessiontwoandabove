﻿using Plumbing;

namespace PlumberImpl
{
    public class Dispatcher
    {
        public void DoPlumbingJob(string jobNumber)
        {
            var notificationService = new NotificationService();
            var plumber = new Plumber(notificationService);
            plumber.DoPlumbing(jobNumber);
        }
    }
}