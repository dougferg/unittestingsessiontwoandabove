﻿namespace Plumbing
{
    public class Pipe
    {
        private readonly string _partNumber;

        public Pipe(string partNumber)
        {
            _partNumber = partNumber;
        }

        public string PartNumber
        {
            get { return _partNumber; }
        }
    }
}