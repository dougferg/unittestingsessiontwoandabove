﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;

namespace Plumbing.Tests
{
    [TestFixture]
    public class PlumberTests
    {
        const string JobNo = "blah";

        private Plumber _sut;
        private Mock<IPipeGetter> _pipeGetter;
        private Mock<INotificationService> _noticationService;

        [SetUp]
        public void Init()
        {
            _pipeGetter = new Mock<IPipeGetter>();
            _pipeGetter.Setup(x => x.GetPipe(JobNo)).Returns(new List<Pipe>() { new Pipe("blahFromMock") });
            _noticationService = new Mock<INotificationService>();
            _sut = GetSut();
        }

        private Plumber GetSut()
        {
            return new Plumber(_pipeGetter.Object, _noticationService.Object);
        }


        [Test]
        public void DoPlumbing_get_pipes()
        {
            //Arrange


            //Act
            _sut.DoPlumbing(JobNo);

            //Assert
            _pipeGetter.Verify(x => x.GetPipe(JobNo));

        }

        [Test]
        public void DoPlumbing_invokes_notificaiton_service()
        {
            //Arrange


            //Act
            _sut.DoPlumbing(JobNo);

            //Assert
            _noticationService.Verify(x => x.SendNotification(JobNo));

        }


    }

}
