﻿using System.Collections.Generic;

namespace Plumbing
{
    public class BobbyErrandBoy
    {
        public IList<Pipe> GetPipe(string partNumber, int quantity)
        {
            var list = new List<Pipe>();
            for (int i = 0; i < quantity; i++)
            {
                list.Add(new Pipe(partNumber));
            }
            return list;
        }
    }
}
