﻿using System;
using System.Collections.Generic;

namespace Plumbing
{
    public class Plumber
    {
        private readonly IPipeGetter _pipeGetter;
        private readonly INotificationService _notificationService;

        public Plumber(IPipeGetter pipeGetter, INotificationService notificationService)
        {
            if (pipeGetter == null) throw new ArgumentNullException("pipeGetter");
            if (notificationService == null) throw new ArgumentNullException("notificationService");
            _pipeGetter = pipeGetter;
            _notificationService = notificationService;
        }

        public void DoPlumbing(string jobNumber)
        {
            var pipe = _pipeGetter.GetPipe(jobNumber);
            //do something here.
            _notificationService.SendNotification(jobNumber);
        }
    }

    public interface IPipeGetter
    {
        IList<Pipe> GetPipe(string jobNumber);
    }

    public interface INotificationService
    {
        void SendNotification(string jobNumber);
    }
}